# Maintainer: Florian Pritz <bluewind@xinu.at>
# Contributor: Phillip Smith <fukawi2@NO-SPAM.gmail.com>

pkgname=postfwd
pkgver=2.03
pkgrel=2
pkgdesc="Combines complex postfix restrictions in a ruleset similar to those of the most firewalls"
arch=(any)
url="https://postfwd.org/"
license=('bsd')
depends=('perl' 'perl-io-multiplex' 'perl-net-server' 'perl-net-dns')
source=("https://postfwd.org/$pkgname-$pkgver.tar.gz"
        "postfwd.sysusers"
        "service")
md5sums=('ab4da91fafdc5054b29202e87928cc1a'
         'e9fab05e2262385b759bd68c580e1f05'
         'ebccbaa8fd20b89da8d7f1ef0e922be3')
sha256sums=('ec1f9e6f7290913fa045132231f897828bc75871f26cc810c15c0031b86e48ce'
            '887272c2df36abf650e5299f47d05717b54180c63928cf11195a0e59a77934c4'
            'dafc97b6a4a26a2a4fc26622003453bd6457b0ae31f02c1394d80b0a411fa9b8')
backup=('etc/postfwd/postfwd.cf')

prepare() {
  cd "$srcdir/$pkgname"

  sed -i bin/postfwd-script.sh \
      -e 's|PFWCMD=/usr/local/postfwd/sbin/postfwd|PFWCMD=/usr/bin/postfwd|g'
}

package() {
  cd "$srcdir/$pkgname"

  install -dm755 "$pkgdir"/{etc/$pkgname,usr/{bin,share/{man,$pkgname}}}

  cp -ra etc/*    "$pkgdir/etc/$pkgname/"
  cp -ra -t "$pkgdir/usr/bin/" bin/* sbin/*
  cp -ra man/*    "$pkgdir/usr/share/man/"
  cp -ra tools/*  "$pkgdir/usr/share/$pkgname/"
  cp -ra doc/*    "$pkgdir/usr/share/$pkgname/"

  install -Dm644 "$srcdir/service" "$pkgdir/usr/lib/systemd/system/${pkgname}.service"
  install -Dm644 "$srcdir/service" "$pkgdir/usr/lib/systemd/system/${pkgname}2.service"
  sed -i 's|/usr/bin/postfwd|/usr/bin/postfwd2|' "$pkgdir/usr/lib/systemd/system/${pkgname}2.service"

  rm "$pkgdir/usr/bin/postfwd.service"

  install -Dm644 "$srcdir/postfwd.sysusers" "$pkgdir/usr/lib/sysusers.d/postfwd.conf"

  install -dm755 "$pkgdir/usr/share/licenses/$pkgname"
  mv "$pkgdir/usr/share/$pkgname/LICENSE" "$pkgdir/usr/share/licenses/$pkgname"
}

# vim:set ts=2 sw=2 et:
